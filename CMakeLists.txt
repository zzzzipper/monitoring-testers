cmake_minimum_required(VERSION 3.5.1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

FIND_PACKAGE( Boost 1.71 COMPONENTS program_options REQUIRED )
INCLUDE_DIRECTORIES( ${Boost_INCLUDE_DIR} )

include_directories(${CMAKE_SOURCE_DIR}/include)


set(SERVER_SOURCE "${CMAKE_SOURCE_DIR}/server/main.cpp"
    "${CMAKE_SOURCE_DIR}/server/dispatcher.cpp"
    "${CMAKE_SOURCE_DIR}/server/processor.cpp"
    "${CMAKE_SOURCE_DIR}/server/tcpsrv.cpp"
    "${CMAKE_SOURCE_DIR}/server/config.cpp"
    "${CMAKE_SOURCE_DIR}/modules/ldd/ldd_library.cpp"
    "${CMAKE_SOURCE_DIR}/modules/ldd/ldd_library_manager.cpp")

set(CLIENT_SOURCE "${CMAKE_SOURCE_DIR}/test/client/main.cpp"
    "${CMAKE_SOURCE_DIR}/test/client/test.cpp")


message(AUTHOR_WARNING ${SERVER_SOURCE})

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

add_executable(server ${SERVER_SOURCE})
add_executable(client ${CLIENT_SOURCE})

target_link_libraries(server
    boost_system
    boost_log
    pthread
    boost_thread
    dl)

target_link_libraries(client
    boost_system
    boost_program_options
    boost_log
    pthread
    boost_thread)
