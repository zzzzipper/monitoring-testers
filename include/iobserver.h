#pragma once

#include "ievent.h"

class IObserver {
    public:
    void send(IEvent& e);
}